import json
import os 
import argparse

class Arguments(object):
    def __init__(self):
        self.args = self.init_arguments()

        # Argparse part 
        self.tablet_path = self.args.set_tp
        self.source_path = self.args.set_sp
        self.tablet_brand = self.args.set_tb

    def init_arguments(self):
        parser = argparse.ArgumentParser(description="Configure sync script for reading tablet")
        parser.add_argument("--set-tp",
                            "--set-tablet-path", 
                            type=str, 
                            help="The current book directory within your tablet",
                            required=True)
        parser.add_argument("--set-sp",
                            "--set-source-path",
                            type=str,
                            help="The current book directory where ebooks is stored localy",
                            required=True)
        parser.add_argument("--set-tb",
                            "--set--tablet-brand",
                            type=str,
                            help="The brand which correspond to the tablet brand or name that appear in the peripheric name")
        return parser.parse_args()


class Config(object):
    def __init__(self, arguments: Arguments, path):
        # Dictionnary
        self.path = path
        self.arguments = arguments
        self.config_file = self.get_config_from_json()

    def store(self):
        if self.json_is_empty():
            with open(self.path, "w") as f:
                json.dump({
                    "config": {
                        "source_path": f"{self.arguments.source_path}",
                        "book_path": f"{self.arguments.tablet_path}",
                        "tablet_brand": f"{self.arguments.tablet_path}"
                    }
                }, f, indent=4)

    def get_config_from_json(self):
        with open(f"{self.path}", "rb") as f:
            return json.loads(f.read())

    def json_is_empty(self):
        with open(f"{self.path}", "rb") as f:
            data = json.loads(f.read())
        return all(bool(item) is False for item in data.get('config').values())

if __name__ == '__main__':
    args = Arguments()
    config = Config(args, "../config.json")
    config.store()